package com.kiki.oauth.controller;

import com.kiki.oauth.utils.UserInfoUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@Slf4j
@RestController
@RequestMapping("/demo")
public class DemoController {

    @RequestMapping(value = "/hello",method = RequestMethod.GET)
    public String hello(){
        Long userId = UserInfoUtil.getUserId();
        String username = UserInfoUtil.getUsername();
        Set<String> authorities = UserInfoUtil.getUserAuthorities();
        log.info("user info userid {} username {} authorities{}",userId,username,authorities);
        return "hello world!";
    }
}
