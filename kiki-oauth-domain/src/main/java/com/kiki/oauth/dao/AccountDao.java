package com.kiki.oauth.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kiki.oauth.entity.Account;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface AccountDao extends BaseMapper<Account> {

    @Select("select * from account where open_code=#{account} and deleted=0 order by created desc limit 1")
    Account selectByAccount(@Param("account")String account);

}
