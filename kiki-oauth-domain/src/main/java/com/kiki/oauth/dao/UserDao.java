package com.kiki.oauth.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kiki.oauth.entity.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface UserDao extends BaseMapper<User> {

    @Select("select * from user where id=#{id} limit 1")
    User selectById(@Param("id")Long id);
}
