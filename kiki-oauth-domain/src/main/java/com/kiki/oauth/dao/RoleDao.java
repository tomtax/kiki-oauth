package com.kiki.oauth.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kiki.oauth.entity.Role;

public interface RoleDao extends BaseMapper<Role> {
}
