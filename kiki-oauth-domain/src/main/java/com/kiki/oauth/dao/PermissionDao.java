package com.kiki.oauth.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kiki.oauth.entity.Permission;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface PermissionDao extends BaseMapper<Permission> {

    @Select("select a.* from permission a inner join role_permission b on a.id=b.permission_id " +
            "inner join user_role c on b.role_id=c.id where c.user_id=#{userId}")
    List<Permission> selectByUserid(@Param("userId") Long userId);
}
