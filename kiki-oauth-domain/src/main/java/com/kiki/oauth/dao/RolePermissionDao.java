package com.kiki.oauth.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.kiki.oauth.entity.RolePermission;

public interface RolePermissionDao extends BaseMapper<RolePermission> {

}
