package com.kiki.oauth.enums;

public enum AuthTokenTypeEnum {

    JWT("jwt","JWT"),

    REDIS("redis","REDIS"),

    JDBC("jdbc","JDBC");

    private String type;

    private String name;

    AuthTokenTypeEnum(String type,String name){
        this.type = type;
        this.name = name;
    }

    public String getType() {
        return type;
    }
}
