package com.kiki.oauth.entity;

import lombok.Data;

/**
 * 用户权限
 */
@Data
public class Permission extends BaseEntity {

    /**
     * 所属父级权限ID
     */
    private Long parentId;

    /**
     * 权限唯一CODE代码
     */
    private String code;

    /**
     * 权限名称
     */
    private String name;

    /**
     * 权限介绍
     */
    private String intro;

    /**
     * 权限类别
     */
    private Integer category;

    /**
     * 权限URI
     */
    private String uri;
}
