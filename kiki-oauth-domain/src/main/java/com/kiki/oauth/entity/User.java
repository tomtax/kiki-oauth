package com.kiki.oauth.entity;

import lombok.Data;

/**
 * 用户
 */
@Data
public class User extends BaseEntity {

    /**
     * 用户状态:0=正常,1=禁用
     */
    private Integer state = 0;

    /**
     * 姓名
     */
    private String name;

    /**
     * 头像地址
     */
    private String headImgUrl;

    /**
     * 手机号码
     */
    private String mobile;

    /**
     * 密码加盐
     */
    private String salt;

    /**
     * 登录密码
     */
    private String password;
}
