package com.kiki.oauth.entity;

import lombok.Data;

/**
 * 用户登录账号
 */
@Data
public class Account extends BaseEntity{

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 登录账号
     */
    private String openCode;

    /**
     * 账号类别
     */
    private Integer category;
}
