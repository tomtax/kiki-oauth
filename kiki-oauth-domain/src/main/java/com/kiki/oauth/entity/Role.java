package com.kiki.oauth.entity;

import lombok.Data;

/**
 * 角色
 */
@Data
public class Role extends BaseEntity {

    /**
     * 角色编码
     */
    private String code;

    /**
     * 角色名称
     */
    private String name;

    /**
     * 角色介绍
     */
    private String intro;
}
