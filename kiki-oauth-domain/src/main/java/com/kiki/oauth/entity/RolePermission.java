package com.kiki.oauth.entity;

import lombok.Data;

/**
 * 角色权限
 */
@Data
public class RolePermission {

    /**
     * 角色ID
     */
    private Long roleId;

    /**
     * 权限ID
     */
    private Long permissionId;
}
