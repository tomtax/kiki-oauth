package com.kiki.oauth.entity;

import lombok.Data;

import java.util.Date;

@Data
public class BaseEntity {

    /**
     * ID
     */
    private Long id;

    /**
     * 创建时间
     */
    private Date created;

    /**
     * 创建者
     */
    private Long creator;

    /**
     * 修改时间
     */
    private Date edited;

    /**
     * 修改者
     */
    private Long editor;

    /**
     * 是否删除 0否 1是
     */
    private Integer deleted = 0;
}
