package com.kiki.oauth.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties("security")
public class SecurityProperties {

    private String type;

    private JwtProperties jwt;

    @Data
    public static class JwtProperties {

        private Resource keyStore;

        private String keyStorePassword;

        private String keyPairAlias;

        private String keyPairPassword;
    }
}
