1.authorization_code模式
http://localhost:8080/oauth/authorize?response_type=code&scope=all&client_id=client&redirect_uri=http://www.baidu.com&state=b375bc2b-25f7-4dce-9b36-5f9e2d20bda1

2.password模式
curl -X POST http://localhost:8080/oauth/token -H "Accept: application/json" -d "grant_type=password&scope=read%20write&client_id=curl-client&client_secret=client-secret&username=nangzi&password=nangzi"

3.client_credentials模式
curl -X POST http://localhost:8080/oauth/token -H "Accept: application/json" -d "grant_type=client_credentials&scope=read%20write&client_id=curl-client&client_secret=client-secret"

4.implicit模式
http://localhost:8080/oauth/authorize?response_type=token&scope=all&client_id=client1&client_secret=secret&redirect_uri=http://www.baidu.com&state=b375bc2b-25f7-4dce-9b36-5f9e2d20bda1

5.refresh_token
curl -X POST http://localhost:8080/oauth/token -H "Accept: application/json" -d "grant_type=refresh_token&refresh_token=0843fbec-20e3-4802-93a0-357488403924&client_id=curl-client&client_secret=client-secret"