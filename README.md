# kiki-oauth

#### 介绍
此项目主要用于学习oauth2，spring5与之前的oauth2版本的实现方式有些区别。本项目是在spring5版本的基础上实现。

#### 软件架构
项目主要基于springboot2.2.4.RELEASE实现。spring security采用2.1.2.RELEASE版本。


#### 使用说明

1.  项目实现了oauth基于jdbc，redis，jwt几种方式的token存储方式。
2.  项目很简单，运行kiki-oauth-auth-server启动授权服务器。kiki-oauth-resource-server是资源服务器，资源请求通过此项目校验请求的是否合法。工程中kiki-oauth-demo项目就是演示实际项目通过依赖kiki-oauth-resource-server来自动配置请求的token有效性验证。
3.  kiki-oauth-domain项目是其他几个项目都会依赖的基础实体类，公共类的实现。

#### 参与贡献

1.  欢迎大家修改和指正代码的优化之处。